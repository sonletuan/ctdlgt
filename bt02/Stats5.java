public class Stats5{
	public static void main(String [] args){
		int n = 5;
		double a = Math.random();
		double b = Math.random();
		double c = Math.random();
		double d = Math.random();
		double e = Math.random();
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		
		double max     = Math.max(a, Math.max(b, Math.max(c, Math.max(d, e))));
	        double min     = Math.min(a, Math.min(b, Math.min(c, Math.min(d, e))));
        	double average = (a + b + c + d + e) / n;

		System.out.println("Average = " + average);
        	System.out.println("Min     = " + min);
        	System.out.println("Max     = " + max);

	}
}