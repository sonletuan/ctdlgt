public class StdGaussian{
	public static void main(String [] args){
		double u = Math.random();
		double v = Math.random();
		double z = Math.sin(2*3.14*v)*Math.sqrt(-2*Math.log(u));
		System.out.println("u= "+u);
		System.out.println("v= "+v);
		System.out.println(z);
	}
}
