
public class Velocity{
	public static void main(String [] args){
		double x0, v0, t, v;
		double g = 9.80072;
		x0 = Double.parseDouble(args[0]); 
		v0 = Double.parseDouble(args[1]);
		t = Double.parseDouble(args[2]);
		v = x0 + v0*t + g*t*t/2;
		System.out.println(v);
	}
}