public class WindChill{
	public static void main(String [] args){
		double Ta = Double.parseDouble(args[0]);
		double V = Double.parseDouble(args[1]);
		Double Twc = 13.12 + 0.6215*Ta - 11.37*Math.pow(V,0.16) + 0.3965*Ta*Math.pow(V,0.16);
		System.out.print(Twc);
	}
}