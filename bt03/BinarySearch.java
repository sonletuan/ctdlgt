package binarysearch;
import java.util.*;

public class BinarySearch {
    public static int indexOf(int[] a, int k ){
        int head = 0;
        int tail = a.length - 1;
        while (head <= tail){
            int mid = head + (tail - head)/2;
            if (k > a[mid]) head = mid + 1;
            else if (k < a[mid]) tail = mid - 1;
            else return mid;
        }
        return -1;
    }
    public static void main(String [] args){
        Scanner Input = new Scanner(System.in);
        System.out.println("Nhap so phan tu mang N: ");
        int n = Input.nextInt();
        int [] s = new int[n];
        for (int i=0; i<n; i++){
            s[i] = Input.nextInt();
        }
        int t;
        for (int i=0; i<n-1; i++){
            for (int j = i+1; j <n; j++){
              if (s[i] > s[j]){
                  t = s[i];
                  s[i] = s[j];
                  s[j] = t;
              }  
            }
        }
        for (int i =0; i<n; i++){
            System.out.printf("%d\t",s[i]);
        }
        System.out.println("\nNhap so can tim: ");
        int k =Input.nextInt();
        System.out.println("Vi tri cua " +k+ " trong mang: " + indexOf(s,k));
    }
}