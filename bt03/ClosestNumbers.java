package closestnumbers;
import java.util.Scanner;
/**
 *
 * @author admin
 */
public class ClosestNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Nhap so phan tu mang ");
        Scanner Input = new Scanner(System.in);
        int n = Input.nextInt();
        int a[] = new int[10];
        for (int i = 0; i<n; i++){
            a[i] = Input.nextInt();
        }
        int min =Math.abs(a[0] - a[1]);
        for (int i = 0; i < n-1; i++){
            for (int j = i+1; j<n;j++){
                if (Math.abs(a[i]-a[j]) < min){
                    min = Math.abs(a[j] - a[i]);
                    
                }
                System.out.println(a[i]+" "+a[j]);
            }
        }
        
    }
    
}

