package pairs;
import java.util.Scanner;
/**
 *
 * @author LeTuanSon
 */
public class Pairs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner Input = new Scanner (System.in);
        int [] a = new int[10];
        System.out.println("Nhap so phan tu day: ");
        int n = Input.nextInt();
        System.out.println("Nhap khoang cach");
        int k = Input.nextInt();
        for (int i = 0; i < n; i++){
            System.out.println("a["+i+"]");
            a[i] = Input.nextInt();    
        }
        int count=0;
        for (int i = 0; i < n-1; i++){
            for(int j = i+1; j < n; j++){
                if(a[j] - a[i] == k || a[j]-a[i]==-k){
                    count++;
                    System.out.println(a[i]+" "+ a[j]);
                }
            }
        }
        System.out.println(count);
    }
    
}
