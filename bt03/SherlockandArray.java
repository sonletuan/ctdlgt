package sherlockandarray;
import java.util.*;
/**
 *
 * @author LeTuanSon
 */
public class SherlockandArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner Input = new Scanner(System.in);
        String output = "No";
        System.out.println("Nhap so phan tu: ");
        int n = Input.nextInt();
        int [] s = new int[n];
        if (n == 1 || n == 2){
            output = "Error!";
            System.out.println(output);
        }
        else{       
            int sum = 0;
            for (int i = 0; i < n; i++){
                s[i] = Input.nextInt();
                sum += s[i]; // tong day
            }
            int left = 0; // tong ben trai s[i]
            int right ; // tong ben phai s[i]
            for (int i = 1; i < n; i++){
                left += s[i-1];
                right = sum - left - s[i]; 
                if (left == right){
                    output = "Yes";
                    break;
                } 
            }
            System.out.println(output);
        }
    }
}
