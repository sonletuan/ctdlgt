Node insert(Node head, int data, int position) {

    Node newNode = new Node();
    newNode.data = data;
    newNode.next = null;

    if(head == null && position != 0) {
        return head;
    } else if(head == null && position == 0) {
        head = newNode;
        return head;
    }

    if(position == 0) {
        newNode.next = head;
        head = newNode;
        return head;
    }

    Node current = head;
    Node previous = null;
    int i = 0;

    for(i = 0; i < position; i++) {
        previous = current;
        current = current.next;
        if(current == null)
            break;
    }

    newNode.next = current;
    previous.next = newNode;
    return head;

}