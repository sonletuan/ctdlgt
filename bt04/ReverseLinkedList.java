Node Reverse(Node head) {
if (head != null) {
        Node prev = null;
        while (head.next != null) {
            Node next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }
        head.next = prev;
    }
    return head;
}